from telegram import Update

from telegram.ext import ContextTypes


async def token(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if len(context.args) == 0:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Token: {context.user_data.get('token', 'None')}.",
        )
        return
    context.user_data["token"] = context.args[0]
    await context.bot.send_message(chat_id=update.effective_chat.id, text="Token set.")